var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3');
var db = new sqlite3.Database('./client', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the chinook database.');
});

/* GET users listing. */
router.get('/', function(req, res) {
  //res.send('respond with a resource');

  res.json([{
  	id:1,
  	username: "samsepi01"
  }, {
  	id:2,
  	username:"D0loresH4ze"
  }])
});

router.get('/db', function(req, res) {
  db.all('SELECT * FROM client', (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    return row
    ? res.json(row)
    : console.log('No clients in base');
  })
});

module.exports = router;
